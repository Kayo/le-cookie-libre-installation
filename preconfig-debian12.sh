#!/bin/bash
# Script de pré-configuration à l'utilisation de Ansible sur les 
# machines auto-hébergés debian pour Le Cookie Libre

# A executer en root
if [ "$(id -u)" -ne 0 ]; then
        echo 'Ce script doit être exécuté en root' >&2
        exit 1
fi

echo "Mise à jour de debian..."
apt-get update && apt-get upgrade -y

echo "Installation des paquets sudo, curl et wget"
apt-get install sudo curl wget

echo "Création des liens symbolique python pour le fonctionnement d'ansible..."
ln -s /usr/bin/python3 /usr/bin/python

echo "Ajout de l'utilisateur jbouchoucha en sudo..."
sudo usermod -a -G sudo jbouchoucha

echo "Ajout de la clé publique SSH pour l'utilisateur jbouchoucha"
mkdir -p /home/jbouchoucha/.ssh/
echo "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAD0TKTU9DESNCjRN73t8S6rJ59txa7SA32CS4e+QnC5SBJTd8q9FxVHqzO+TDLxNaum1hMrSUgHnX5os51LiQs6qQBkZdtSiayYp+Ifl6VPdYA6dC00Q7Kl0+7ebiWpjVi7D2v7mtkjXK8a1sKmZ2R1QIWRY+ljIxmqFIwSM5v9eJDNhw==" >> /home/jbouchoucha/.ssh/authorized_keys

##################################################
# Tailscale installation (https://tailscale.com/kb/1174/install-debian-bookworm) - ANSIBLABLE
##################################################

curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null
curl -fsSL https://pkgs.tailscale.com/stable/debian/bookworm.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list

sudo apt-get update && sudo apt-get install tailscale -y

echo "Aller sur https://headscale.lecookielibre.fr et ajouter un nouvel utilisateur du nom de la machine local. Puis créer une clé d'authentification réutilisable. Ensuite, il ne reste qu'à se connecter au VPN via la commande ci-dessous"
echo
echo "tailscale up --login-server http://headscale.lecookielibre.fr:8080 --accept-routes --authkey <PREAUTH_KEY>"
echo
